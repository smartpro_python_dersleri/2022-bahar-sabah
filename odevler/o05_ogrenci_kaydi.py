"""
Bu projede 3 tane classımız olacak
Öğrenci, Öğretmen, Sınıf

Sınıf içeriği:
    * Öğrenciler: Öğrenci objesini liste olarak alacak.
    * Öğretmen: Öğretmen objesi direkt olacak
    * Konu
    * Sınıf oda numarası

    * Öğrenci ekleme
    * Öğretmen değiştirme
    * Öğrenci ve not ortalamalarını listeleme

Öğrenci içeriği:
    * ad-soyad
    * yaş
    * cinsiyet
    * notlar

    * Not ekleme
    * yaşlandırma
    * Not ortalaması alma
    * Geçme durumunu kontrol etme

Öğretmen içeriği:
    * ad-soyad
    * yaş
    * cinsiyet
    * uzmanlık alanları

    * uzmanlık alanı ekleme
    * yaşlandırma
"""
