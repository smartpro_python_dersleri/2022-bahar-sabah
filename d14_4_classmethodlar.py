class Coffee:
    def __init__(self, name: str, origin: str, roast: str, is_hot: bool, additions: list[str]) -> None:
        self.name = name
        self.origin = origin
        self.roast = roast
        self.is_hot = is_hot
        self.additions = additions

    def __str__(self) -> str:
        return self.name

    def get_additions(self) -> str:
        return ', '.join(self.additions)

    @classmethod
    def create_coffee(cls, origin: str, roast: str, additions: list[str], is_hot: bool = True) -> 'Coffee':
        if roast not in ['blonde', 'medium', 'dark']:
            raise ValueError('Abi bana duzgun bir roast ver')
        name = f'{origin} - {roast}'
        return cls(name, origin, roast, is_hot, additions)


def main() -> None:
    honduras = Coffee.create_coffee('Honduras', 'medium', [], is_hot=True)
    print(honduras)
    print(honduras.get_additions())
    kenya = honduras.create_coffee('Kenya', 'dark', ['milk'], is_hot=True)
    print(kenya)
    print(kenya.get_additions())


if __name__ == "__main__":
    main()
